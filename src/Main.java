import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        var a = scanner.nextDouble();
        var b = scanner.nextDouble();

        System.out.println("Sum:\t\t\t" + (a + b));
        System.out.println("Difference:\t\t" + (a - b));
        System.out.println("Product:\t\t" + (a * b));
        System.out.println("Quotient:\t\t" + (a / b));
    }
}